## About this Project

The server port is configured to port 2000 and if you need to access to H2 database console

Please use below URL after you have start the application.

# H2 Embedded Database URL: http://localhost:2000/h2/

# The jar file can be found in target folder and the jar file name is moneylion.humairah.test2-0.0.1-SNAPSHOT.jar

To run this jar file, jar file need to be in your current directory example if your current directory is within the source code:

Example: C:\Users\User\git\humairah-assessment\moneylion.humairah.test2\target

you can use below command to start the application server:

# java -jar moneylion.humairah.test2-0.0.1-SNAPSHOT.jar