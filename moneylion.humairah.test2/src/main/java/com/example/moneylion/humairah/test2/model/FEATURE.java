package com.example.moneylion.humairah.test2.model;  
import javax.persistence.Column;  
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;  
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

//mark class as an Entity   
@Entity  
//defining class name as Table name  
@Table  
//@JsonIgnoreProperties(value = { "featureName", "id", "email", "enable" })
public class FEATURE {
	
//mark id as primary key
@Id
@GeneratedValue(
	strategy= GenerationType.AUTO,
	generator="native"
)
//defining id as column name
@Column
private int id;

//defining featureName as column name
@Column
private String featureName;

//defining email as column name
@Column
private String email;

//defining access as column name
@Column
private Boolean enable;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

@JsonProperty(value="featureName")
public String getFeatureName() {
	return featureName;
}

public void setFeatureName(String featureName) {
	this.featureName = featureName;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public Boolean getEnable() {
	return enable;
}

public void setEnable(Boolean enable) {
	this.enable = enable;
}
}
