package com.example.moneylion.humairah.test2.controller;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;  
import org.springframework.web.bind.annotation.PostMapping;  
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;  
import com.example.moneylion.humairah.test2.model.FEATURE;  
import com.example.moneylion.humairah.test2.service.ApplicationService;  
@RestController
@RequestMapping("/moneylion")
public class ApplicationController {
	//autowired the ApplicationService class
	@Autowired 
	ApplicationService applicationService;
	
	String canAccess = null;
	
	//get mapping for feature name and email  	
	@GetMapping("/feature")
	@ResponseBody()
	public ResponseEntity<Object> getFeature(@RequestParam("email") String email, @RequestParam("featureName") String featureName) {
		FEATURE feature = applicationService.getApplication(email, featureName);
		
		boolean found = true;
		if(feature == null) {
			found = false;
		}
		if(found) {
			HashMap<String, Object> map = getFeature2(feature);
			return new ResponseEntity<Object>(map, HttpStatus.OK);
			
		}else {
			return ResponseEntity.notFound().build();
		}		
	}
	
	private HashMap<String, Object> getFeature2(FEATURE feature) {
		HashMap<String, Object> map = new HashMap<>();
		map.put("canAccess", feature.getEnable());
		
		return map;
	}
	
	//postMapping for feature name and email
	@PostMapping("/feature")	
	private ResponseEntity<String> saveFeature(@RequestBody FEATURE feature) {
		FEATURE createRecord = applicationService.saveOrUpdate(feature);
		ResponseEntity<String> responseEntity;
		if(createRecord != null) {
			responseEntity = new ResponseEntity<String>(HttpStatus.OK);
		}else
			responseEntity = new ResponseEntity<String>(HttpStatus.NOT_MODIFIED);
		
		return responseEntity;
		
	}


	
	
	
}
