package com.example.moneylion.humairah.test2.service;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Service;  
import com.example.moneylion.humairah.test2.model.FEATURE;
import com.example.moneylion.humairah.test2.repository.ApplicationRepository;
@Service
public class ApplicationService {

	@Autowired
	ApplicationRepository applicationRepo;
	
	public FEATURE getApplication(String email,String featureName) {
		return applicationRepo.findUserFeature(email, featureName);
	}
	
	 
	
	public FEATURE saveOrUpdate(FEATURE feature) {
		
		if(feature.getEnable() != null || feature.getEmail() != null || feature.getFeatureName() != null) {
			return applicationRepo.save(feature);
		}else
			return null;		
	}
	
	
}
