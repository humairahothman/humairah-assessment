package com.example.moneylion.humairah.test2.repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.example.moneylion.humairah.test2.model.FEATURE;  
public interface ApplicationRepository extends CrudRepository<FEATURE,Long>{

	@Query("select u from FEATURE u where u.email =:email and u.featureName =:featureName")
	public FEATURE findUserFeature(@Param("email") String email,@Param("featureName") String featureName);

}
